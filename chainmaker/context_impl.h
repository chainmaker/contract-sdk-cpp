/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef CHAINMAKER_CONTEXT_IMPL_H
#define CHAINMAKER_CONTEXT_IMPL_H
#include "chainmaker/chainmaker.h"
namespace chainmaker
{
    class ContextImpl : public Context
    {
    public:
        ContextImpl();
        ~ContextImpl() override;
        bool arg(const std::string &name, std::string &value) override;
        bool get_object(const std::string &key, std::string *value) override;
        bool put_object(const std::string &key, const std::string &value) override;
        bool delete_object(const std::string &key) override;
        void success(const std::string &body) override;
        void error(const std::string &body) override;
        std::unique_ptr<Iterator> new_iterator(const std::string &start, const std::string &limit) override;
        bool call(const std::string &contract,
                          const std::string &method,
                          EasyCodecItems *args,
                          std::string *resp) override;
        void log(const std::string &body) override;

        bool emit_event(const std::string &topic, int data_amount, std::string data, ...) override;

    private:
        EasyCodecItems *_call_args;
    };

}

#endif
