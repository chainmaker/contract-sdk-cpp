/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef CHAINMAKER_CHAINMAKER_H
#define CHAINMAKER_CHAINMAKER_H

#include <map>
#include <memory>
#include <string>
#include <vector>
#include "chainmaker/basic_iterator.h"
#include "chainmaker/easycodec.h"

namespace chainmaker {

#define WASM_EXPORT extern "C" __attribute__((used))
#define SUCCESS 0
#define ERROR 1
#define MAX_CONTRACT_TOPIC_WITH_EVENT_DATA_LEN 17

    class Context {
    public:
        virtual ~Context() {}

        virtual bool arg(const std::string &name, std::string &value) = 0;

        virtual bool get_object(const std::string &key, std::string *value) = 0;

        virtual bool put_object(const std::string &key, const std::string &value) = 0;

        virtual bool delete_object(const std::string &key) = 0;

        virtual void success(const std::string &body) = 0;

        virtual void error(const std::string &body) = 0;

        virtual std::unique_ptr <Iterator> new_iterator(const std::string &start, const std::string &limit) = 0;

        virtual bool emit_event(const std::string &topic, int data_amount, const std::string data, ...) = 0;

        virtual bool call(const std::string &contract,
                          const std::string &method,
                          EasyCodecItems *args,
                          std::string *resp) = 0;
        virtual void log(const std::string &body) = 0;
    };

    class Contract {
    public:
        Contract();

        virtual ~Contract();

        Context *context() {
            return _ctx;
        };

    private:
        Context *_ctx;
    };

}
#endif
