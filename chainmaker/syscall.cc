/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "chainmaker/syscall.h"
#include <string>
#include <string.h>

extern "C" uint32_t fetch_response(char *response, uint32_t response_len);
extern "C" uint32_t call_method(const char *method, uint32_t method_len,
                                const char *request, uint32_t request_len,
                                char *response, uint32_t response_len,
                                uint32_t *success);

namespace chainmaker
{

    static bool syscall_raw(const std::string &method,
                            const char *request, uint32_t len, std::string *response)
    {
        char buf[1024];
        uint32_t buf_len = sizeof(buf);

        uint32_t response_len = 0;
        uint32_t success = 0;

        response_len = call_method(method.data(), uint32_t(method.size()),
                                   request, len,
                                   &buf[0], buf_len, &success);
        // method has no return and no error
        if (response_len <= 0)
        {
            return true;
        }

        // buf can hold the response
        if (response_len <= buf_len)
        {
            response->assign(buf, response_len);
            return success == 1;
        }

        // slow path
        response->resize(response_len + 1, 0);
        success = fetch_response(&(*response)[0u], response_len);
        return success == 1;
    }

    bool syscall(const std::string &method,
                 const byte *request, uint32_t len, std::string *response)
    {
        bool ok = syscall_raw(method, (char *)request, len, response);
        if (!ok)
        {
            return false;
        }
        return true;
    }
}
