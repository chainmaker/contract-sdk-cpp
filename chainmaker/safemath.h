/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#pragma once

// TODO: this just call the system assert, will implement in near future.
namespace chainmaker
{
    bool safe_assert(bool value)
    {
        assert(value);
        return true;
    };
}
