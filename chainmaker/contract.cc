/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "chainmaker/context_impl.h"
#include "chainmaker/syscall.h"
#include "chainmaker/chainmaker.h"

namespace chainmaker
{

    Contract::Contract()
    {
        _ctx = new ContextImpl();
    }

    Contract::~Contract()
    {
        delete (_ctx);
    }
}
