/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "chainmaker/context_impl.h"
#include "chainmaker/syscall.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include "chainmaker/chainmaker.h"

namespace chainmaker {

    ContextImpl::ContextImpl() {
        byte *req = nullptr;
        uint32_t len = 0;
        std::string resp;
        syscall("GetCallArgs", req, len, &resp);
        auto *resp_items = easy_unmarshal((byte *) resp.data());
        _call_args = new EasyCodecItems(resp_items->len, resp_items->ecitems);
    }

    ContextImpl::~ContextImpl() = default;

    bool ContextImpl::arg(const std::string &name, std::string &value) {
        if (_call_args == nullptr) {
            return false;
        }
        for (int i = 0; i < _call_args->len; i++) {
            auto arg_pair = _call_args->ecitems[i];
            if (arg_pair.key == name) {
                value = arg_pair.value;
                return true;
            }
        }
        return false;
    }

    bool ContextImpl::get_object(const std::string &key, std::string *value) {
        EasyCodecItems *req;
        easy_codec_item key_item;
        key_item.key = (char *) "key";
        key_item.key_type = easy_key_type_user;
        key_item.value_type = easy_value_type_string;
        key_item.value = key;
        easy_codec_item items[] = {key_item};
        req = new EasyCodecItems(1, items);
        uint32_t len;
        byte *reqBytes = easy_marshal(req, &len);
        bool ok = syscall("GetObject", reqBytes, len, value);
        delete (req);
        if (!ok) {
            return false;
        }
        return true;
    }

    bool ContextImpl::put_object(const std::string &key, const std::string &value) {
        EasyCodecItems *req;
        easy_codec_item key_item;
        key_item.key = "key";
        key_item.key_type = easy_key_type_user;
        key_item.value_type = easy_value_type_string;
        key_item.value = key;
        easy_codec_item value_item;
        value_item.key = "value";
        value_item.key_type = easy_key_type_user;
        value_item.value_type = easy_value_type_string;
        value_item.value = value;
        easy_codec_item items[] = {key_item, value_item};
        req = new EasyCodecItems(2, items);
        uint32_t len;
        byte *reqBytes = easy_marshal(req, &len);
        std::string resp;
        bool ok = syscall("PutObject", reqBytes, len, &resp);
        delete (req);
        if (!ok) {
            return false;
        }
        return true;
    }


    bool ContextImpl::emit_event(const std::string &topic, int data_amount, const std::string data, ...) {
        if (data_amount > MAX_CONTRACT_TOPIC_WITH_EVENT_DATA_LEN - 1 || data_amount < 1) {
            return false;
        }
        EasyCodecItems *req;
        auto *items = new easy_codec_item[data_amount + 1];
        easy_codec_item key_item;
        key_item.key = "topic";
        key_item.key_type = easy_key_type_user;
        key_item.value_type = easy_value_type_string;
        key_item.value = topic;
        items[0] = key_item;

        va_list ap;
        va_start(ap, data);
        easy_codec_item data_item_full;
        data_item_full.key = "data1";
        data_item_full.key_type = easy_key_type_user;
        data_item_full.value_type = easy_value_type_string;
        data_item_full.value = data;
        items[1] = data_item_full;
        for (int i = 1; i < data_amount; i++) {
            char *part_data = va_arg(ap, char * );
            if (part_data == nullptr) {
                break;
            }
            easy_codec_item data_item;
            std::string key;
            key = "data" + std::to_string(i + 1);
            data_item.key = key;
            data_item.key_type = easy_key_type_user;
            data_item.value_type = easy_value_type_string;
            data_item.value = part_data;
            items[i + 1] = data_item;
        }
        va_end(ap);
        req = new EasyCodecItems(data_amount + 1, items);
        uint32_t len;
        byte *reqBytes = easy_marshal(req, &len);
        std::string resp;
        bool ok = syscall("EmitEvent", reqBytes, len, &resp);
        delete (req);
        delete[] items;
        if (!ok) {
            return false;
        }
        return true;
    }

    bool ContextImpl::delete_object(const std::string &key) {
        easy_codec_item key_item;
        key_item.key = "key";
        key_item.key_type = easy_key_type_user;
        key_item.value_type = easy_value_type_string;
        key_item.value = key;
        easy_codec_item items[] = {key_item};
        auto *req = new EasyCodecItems(1, items);
        uint32_t len;
        byte *reqBytes = easy_marshal(req, &len);
        std::string resp;
        bool ok = syscall("DeleteObject", reqBytes, len, &resp);
        delete (req);
        if (!ok) {
            return false;
        }
        return true;
    }

    void ContextImpl::success(const std::string &body) {
        int32_t success_code = 0;
        easy_codec_item code_item;
        code_item.key = "code";
        code_item.key_type = easy_key_type_user;
        code_item.value_type = easy_value_type_int32;
        code_item.value.resize(sizeof(int32_t));
        memcpy(&code_item.value[0], &success_code, sizeof(int32_t));
        easy_codec_item result_item;
        result_item.key = "result";
        result_item.key_type = easy_key_type_user;
        result_item.value_type = easy_value_type_string;
        result_item.value = body;
        easy_codec_item items[] = {code_item, result_item};
        auto *req = new EasyCodecItems(2, items);
        log("response result: " + req->get_value("result"));
        uint32_t len = 0;
        byte *resp_bytes = easy_marshal(req, &len);
        std::string res;
        syscall("SetOutput", resp_bytes, len, &res);
        delete req;
    }

    void ContextImpl::error(const std::string &body) {
        int32_t error_code = 1;
        easy_codec_item code_item;
        code_item.key = (char *) "code";
        code_item.key_type = easy_key_type_user;
        code_item.value_type = easy_value_type_int32;
        code_item.value.resize(sizeof(int32_t));
        memcpy(&code_item.value[0], &error_code, sizeof(int32_t));
        easy_codec_item msg_item;
        msg_item.key = (char *) "msg";
        msg_item.key_type = easy_key_type_user;
        msg_item.value_type = easy_value_type_string;
        msg_item.value = body;
        easy_codec_item items[] = {code_item, msg_item};
        auto *req = new EasyCodecItems(2, items);
        log("response result: " + req->get_value("result"));
        uint32_t len = 0;
        byte *resp_bytes = easy_marshal(req, &len);
        std::string res;
        syscall("SetOutput", resp_bytes, len, &res);
        delete req;
    }

    std::unique_ptr <Iterator> ContextImpl::new_iterator(const std::string &start, const std::string &limit) {
        return std::unique_ptr<Iterator>(new Iterator(start, limit, ITERATOR_BATCH_SIZE));
    }

    bool ContextImpl::call(const std::string &contract,
                           const std::string &method,
                           EasyCodecItems *args,
                           std::string *resp) {

        EasyCodecItems *request;
        uint32_t len;
        easy_codec_item contract_item;
        contract_item.key = "contract";
        contract_item.key_type = easy_key_type_user;
        contract_item.value_type = easy_value_type_string;
        contract_item.value = contract;
        easy_codec_item method_item;
        method_item.key = "method";
        method_item.key_type = easy_key_type_user;
        method_item.value_type = easy_value_type_string;
        method_item.value = method;
        byte *arg_bytes = easy_marshal(args, &len);

        easy_codec_item args_item;
        args_item.key = "args";
        args_item.key_type = easy_key_type_user;
        args_item.value_type = easy_value_type_bytes;
        args_item.value.resize(len);
        memcpy(&args_item.value[0], arg_bytes, len);
        easy_codec_item items[] = {contract_item, method_item, args_item};
        request = new EasyCodecItems(3, items);
        byte *reqBytes = easy_marshal(request, &len);
        bool ok = syscall("ContractCall", reqBytes, len, resp);
        delete (args);
        delete (request);
        if (!ok) {
            return false;
        }
        return true;
    }

    void ContextImpl::log(const std::string &msg) {
        easy_codec_item msg_item;
        msg_item.key = "msg";
        msg_item.key_type = easy_key_type_user;
        msg_item.value_type = easy_value_type_string;
        msg_item.value = msg;
        easy_codec_item items[] = {msg_item};
        auto *req = new EasyCodecItems(1, items);
        uint32_t len;
        byte *reqBytes = easy_marshal(req, &len);
        std::string resp;
        syscall("LogMsg", reqBytes, len, &resp);
        delete (req);
    }
}