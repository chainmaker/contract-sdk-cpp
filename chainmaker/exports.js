/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

// Tell compiler we have those functions, do not print error when linking
mergeInto(LibraryManager.library, {
    call_method: function () {
    },
    fetch_response: function () {
    },
    wxvm_hash: function () {
    },
    wxvm_encode: function () {
    },
    wxvm_decode: function () {
    },
});
