CXXFLAGS ?= -std=c++11 -Os -I/usr/local/include -Isrc -Werror=vla -I.
LDFLAGS ?= -Oz -s TOTAL_STACK=256KB -s TOTAL_MEMORY=1MB -s DETERMINISTIC=1 -s EXTRA_EXPORTED_RUNTIME_METHODS=["stackAlloc"] -L/usr/local/lib -lprotobuf-lite -lpthread --js-library chainmaker/exports.js

.PHONY: all build clean

all: build

clean:
	$(RM) -r build *.wasm *.log*  tmp

build/main.o: main.cc
	@mkdir -p $(dir $@)
	@echo CC $(notdir $<)
	@$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@

build/basic_iterator.o: chainmaker/basic_iterator.cc
	@mkdir -p $(dir $@)
	@echo CC $(notdir $<)
	@$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@

build/context_impl.o: chainmaker/context_impl.cc
	@mkdir -p $(dir $@)
	@echo CC $(notdir $<)
	@$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@

build/contract.o: chainmaker/contract.cc
	@mkdir -p $(dir $@)
	@echo CC $(notdir $<)
	@$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@

build/crypto.o: chainmaker/crypto.cc
	@mkdir -p $(dir $@)
	@echo CC $(notdir $<)
	@$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@

build/syscall.o: chainmaker/syscall.cc
	@mkdir -p $(dir $@)
	@echo CC $(notdir $<)
	@$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@

build/easycodec.o: chainmaker/easycodec.cc
	@mkdir -p $(dir $@)
	@echo CC $(notdir $<)
	@$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@

main.wasm: build/main.o build/basic_iterator.o build/context_impl.o build/contract.o build/crypto.o build/syscall.o build/easycodec.o
	@echo @$(CXX) -o $@ $^ $(LDFLAGS)
	@$(CXX) -o $@ $^ $(LDFLAGS)

build: main.wasm