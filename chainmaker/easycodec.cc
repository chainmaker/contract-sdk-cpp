/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "chainmaker/easycodec.h"
#include "chainmaker/chainmaker.h"
#include <string.h>
using namespace std;
namespace chainmaker
{
    MarshalBuffer::MarshalBuffer()
    {
        buf = (byte *)malloc(sizeof(byte) * SMALL_BUFFER_SIZE);
        off = -1;
        buf_cap = SMALL_BUFFER_SIZE;
        buf_len = 0;
    }
    MarshalBuffer::MarshalBuffer(byte *bytes)
    {
        off = -1;
        buf = bytes;
    }
    MarshalBuffer::~MarshalBuffer()
    {
        free(buf);
        buf = nullptr;
    }

    byte *MarshalBuffer::bytes()
    {
        return buf;
    }
    int MarshalBuffer::len() const
    {
        return buf_len;
    }
    void MarshalBuffer::write(const byte *bytes, int len)
    {
        this->grow(len);
        for (int i = 0; i < len; buf_len++, i++)
        {
            buf[buf_len] = bytes[i];
        }
    }
    void MarshalBuffer::write_uint(const byte *bytes)
    {
        this->grow(4);
        for (int i = 0; i < 4; buf_len++, i++)
        {
            buf[buf_len] = bytes[i];
        }
    }

    string MarshalBuffer::read_uint()
    {
        string bytes(4, 0);
        for (int i = 0; i < 4; i++, off++)
        {
            bytes[i] = buf[off + 1];
        }
        return bytes;
    }

    string MarshalBuffer::read_str(int read_len)
    {
        string str(read_len, 0);
        for (int i = 0; i < read_len; i++, off++)
        {
            str[i] = buf[off + 1];
        }
        return str;
    }
    string MarshalBuffer::read_bytes(int read_len)
    {
        string str(read_len, 0);
        for (int i = 0; i < read_len; i++, off++)
        {
            str[i] = buf[off + 1];
        }
        return str;
    }
    void MarshalBuffer::grow(int len)
    {
        if (len > buf_cap / 2 - buf_len)
        {
            buf_cap = buf_cap * 2 + len;
            byte *new_buf = (byte *)malloc(sizeof(byte) * buf_cap);
            int i;
            for (i = 0; i < buf_len; i++)
            {
                new_buf[i] = buf[i];
            }
            free(buf);
            buf = new_buf;
            new_buf = nullptr;
        }
    }

    EasyCodecItems::EasyCodecItems()
    {
        this->len = 0;
        ecitems = nullptr;
    }
    EasyCodecItems::EasyCodecItems(int len)
    {
        this->len = len;
        ecitems = (easy_codec_item *)malloc(sizeof(easy_codec_item) * len);
    }

    EasyCodecItems::EasyCodecItems(int len, easy_codec_item *items)
    {
        this->len = len;
        this->ecitems = items;
    }
    EasyCodecItems::~EasyCodecItems()
    {
        // the ecitems maybe stack memory, so don't free it, the vm instance will free all memory of the contract used
        // free(ecitems);
        ecitems = nullptr;
    }
    string EasyCodecItems::get_value(const std::string& key) const
    {
        string value;
        int ok = 0;
        std::string item_key;
        for (int i = 0; i < this->len; i++)
        {
            item_key = ecitems[i].key;

            if (item_key == key)
            {
                ok = 1;
                value = ecitems[i].value;
                break;
            }
        }
        if (ok == 0)
        {
            return "";
        }
        return value;
    }
    byte *binary_uint32_marshal(uint32_t data)
    {
        byte *data_bytes;
        data_bytes = (byte *)malloc(sizeof(byte) * 4);
        //data_bytes[3] = data >> 24;
        //data_bytes[2] = ((data << 8) >> 24);
        //data_bytes[1] = ((data << 16) >> 24);
        //data_bytes[0] = ((data << 24) >> 24);
        memcpy(data_bytes, &data, 4);
        return data_bytes;
    }
    uint32_t binary_uint32_unmarshal(MarshalBuffer *buffer)
    {
        string bytes = buffer->read_uint();
        //uint32_t tmp;
        uint32_t data;
        //data = 0;
        //int i;
        //for (i = 0; i < 4; i++)
        //{
        //    tmp = (uint32_t)(bytes[i] << (8 * i));
        //    data = data | tmp;
        //}
        memcpy(&data, bytes.data(),4);
        return data;
    }
    byte *easy_marshal(EasyCodecItems *items, uint32_t *len)
    {
        //Bytes bytes;
        auto *buffer = new MarshalBuffer();
        buffer->write_uint(binary_uint32_marshal(items->len));
        for (int i = 0; i < items->len; i++)
        {
            if (items->ecitems[i].key_type != easy_key_type_user && items->ecitems[i].key_type != easy_key_type_system)
            {
                continue;
            }
            buffer->write_uint(binary_uint32_marshal(items->ecitems[i].key_type));
            buffer->write_uint(binary_uint32_marshal(items->ecitems[i].key.size()));
            buffer->write((byte*)items->ecitems[i].key.data(), items->ecitems[i].key.size());
            switch (items->ecitems[i].value_type)
            {
            case easy_value_type_int32:
                buffer->write_uint(binary_uint32_marshal(items->ecitems[i].value_type));
                buffer->write_uint(binary_uint32_marshal(4));
                buffer->write(binary_uint32_marshal(*(uint32_t *)items->ecitems[i].value.data()), 4);
                break;
            case easy_value_type_string:
                buffer->write_uint(binary_uint32_marshal(items->ecitems[i].value_type));
                buffer->write_uint(binary_uint32_marshal(items->ecitems[i].value.size()));
                buffer->write((byte *)(items->ecitems[i].value.data()), items->ecitems[i].value.size());
                break;
            case easy_value_type_bytes:
                buffer->write_uint(binary_uint32_marshal(items->ecitems[i].value_type));
                buffer->write_uint(binary_uint32_marshal(items->ecitems[i].value.size()));
                buffer->write((byte *)(items->ecitems[i].value.data()), items->ecitems[i].value.size());
                break;
            }
        }
        *len = (uint32_t)(buffer->len());
        return buffer->bytes();
    }
    EasyCodecItems *easy_unmarshal(byte *data)
    {
        auto *buffer = new MarshalBuffer(data);
        uint32_t count = binary_uint32_unmarshal(buffer);
        auto *items = new EasyCodecItems(count);

        uint32_t key_length;
        uint32_t value_length;

        for (int i = 0; i < count; i++)
        {
            easy_codec_item item;
            //key part
            item.key_type = binary_uint32_unmarshal(buffer);
            key_length = binary_uint32_unmarshal(buffer);
            item.key = buffer->read_str(key_length);
            //value part
            item.value_type = binary_uint32_unmarshal(buffer);
            value_length = binary_uint32_unmarshal(buffer);

            switch (item.value_type)
            {
            case easy_value_type_int32:{
                uint32_t iv = binary_uint32_unmarshal(buffer);
                item.value.resize(sizeof(uint32_t));
                memcpy(&item.value[0], &iv, sizeof(uint32_t));
                break;
            }
            case easy_value_type_string:
                item.value = buffer->read_str(value_length);
                break;
            case easy_value_type_bytes:
                item.value = buffer->read_bytes(value_length);
                break;
            }
            items->ecitems[i] = item;
        }
        return items;
    }
}
