/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include <stdint.h>
#include <string>
#include "chainmaker/easycodec.h"
namespace chainmaker
{

    bool syscall(const std::string &method,
                 const byte *request, uint32_t len, std::string *response);
}
