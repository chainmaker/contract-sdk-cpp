/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "chainmaker/crypto.h"
#include <string>

extern "C" void wxvm_hash(const char *name, const char *inputptr, int inputlen,
                          char *outputptr, int outputlen);
extern "C" void wxvm_encode(const char *name, const char *inputptr, int inputlen,
                            char **outputpptr, int *outlen);
extern "C" int wxvm_decode(const char *name, const char *inputptr, int inputlen,
                           char **outputpptr, int *outlen);

namespace chainmaker
{
    namespace crypto
    {
        std::string sha256(const std::string &input)
        {
            char out[32];
            wxvm_hash("sha256", (const char *)&input[0], input.size(), out, sizeof(out));
            return std::string(out, sizeof(out));
        }

        std::string hex_encode(const std::string &input)
        {
            char *out = NULL;
            int outlen = 0;
            wxvm_encode("hex", (const char *)&input[0], input.size(), &out, &outlen);
            std::string ret(out, outlen);
            free(out);
            return ret;
        }

        bool hex_decode(const std::string &input, std::string *output)
        {
            char *out = NULL;
            int outlen = 0;
            int ret = 0;
            ret = wxvm_decode("hex", (const char *)&input[0], input.size(), &out, &outlen);
            if (ret != 0)
            {
                return false;
            }
            output->assign(out, outlen);
            free(out);
            return true;
        }

    } // namespace crypto
} // namespace chainmaker