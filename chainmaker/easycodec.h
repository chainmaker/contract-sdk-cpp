/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef CHAINMAKER_EASYCODE_H
#define CHAINMAKER_EASYCODE_H
#include <stdlib.h>
#include <stdint.h>
#include <string>
using namespace std;
#define SMALL_BUFFER_SIZE 32
namespace chainmaker
{
    typedef uint32_t easy_key_type;
    typedef uint32_t easy_value_type;

    const easy_key_type easy_key_type_system = 0;
    const easy_key_type easy_key_type_user = 1;

    const easy_value_type easy_value_type_int32 = 0;
    const easy_value_type easy_value_type_string = 1;
    const easy_value_type easy_value_type_bytes = 2;
    typedef unsigned char byte;
    struct easy_codec_item
    {
        easy_key_type key_type;
        string key;

        easy_value_type value_type;
        string value;
    };
    class MarshalBuffer
    {
    private:
        byte *buf;
        int off;
        int buf_cap{};
        int buf_len{};

    public:
        MarshalBuffer();
        explicit MarshalBuffer(byte *bytes);
        ~MarshalBuffer();
        byte *bytes();
        void write(const byte *bytes, int len);
        void write_uint(const byte *bytes);
        string read_uint();
        string read_str(int read_len);
        string read_bytes(int read_len);
        void grow(int len);
        int len() const;
    };
    class EasyCodecItems
    {
    public:
        easy_codec_item *ecitems;
        int len;
        EasyCodecItems();
        explicit EasyCodecItems(int len);
        EasyCodecItems(int len, easy_codec_item *items);
        ~EasyCodecItems();
        string get_value(const std::string& key) const;
    };

    byte *easy_marshal(EasyCodecItems *items, uint32_t *len);
    EasyCodecItems *easy_unmarshal(byte *data);
}
#endif