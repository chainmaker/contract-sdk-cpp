/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "chainmaker/chainmaker.h"
#include "chainmaker/basic_iterator.h"
#include "chainmaker/syscall.h"
#include <string.h>
namespace chainmaker
{
    Iterator::Iterator(const std::string &start, const std::string &limit, size_t cap)
    {
        _cap = cap;
        _start = start;
        _limit = limit;

        if (!load())
        {
            _it = -1;
        }
        else
        {
            _it = 0;
        }
    }

    bool Iterator::load()
    {
        _buf.clear();
        if (_start == _limit)
        {
            return false;
        }
        //每次多取一个作为下一次的主键
        bool ok = range_query(_start, _limit, _cap + 1, &_buf);
        if (!ok)
        {
            error = Error(chainmaker::ErrorType::kErrIteratorLoad);
            return false;
        }
        if (_buf.size() != 0)
        {
            if (_buf.size() == _cap + 1)
            {
                _start = (*(_buf.rbegin())).first;
                _buf.pop_back();
            }
            else
            {
                _start = _limit;
                //这个时候就已经结束了. 但是留到下次取的时候返回false
            }
        }
        return true;
    }

    bool Iterator::next()
    {
        bool ret = end();
        if (ret)
        {
            //只有最后一批结束的时候，才会走到这里
            return false;
        }
        _cur_elem = &_buf[_it];
        ++_it;
        if (end())
        {
            _last_one = *_cur_elem;
            _cur_elem = &_last_one;
            if (!load())
            {
                _it = -1;
            }
            else
            {
                _it = 0;
            }
        }
        return !ret;
    }

    bool Iterator::get(ElemType *t)
    {
        *t = *_cur_elem;
        return true;
    }

    bool Iterator::end()
    {
        return _it >= _buf.size() || _it < 0;
    }

    bool Iterator::range_query(const std::string &s, const std::string &e,
                               const size_t limit, std::vector<std::pair<std::string, std::string>> *res)
    {
        EasyCodecItems *req;
        easy_codec_item limit_item;
        limit_item.key = "limit";
        limit_item.key_type = easy_key_type_system;
        limit_item.value_type = easy_value_type_string;
        limit_item.value = e;
        easy_codec_item start_item;
        start_item.key = "start";
        start_item.key_type = easy_key_type_system;
        start_item.value_type = easy_value_type_string;
        start_item.value = s;
        easy_codec_item cap_item;
        cap_item.key = "cap";
        cap_item.key_type = easy_key_type_system;
        cap_item.value_type = easy_value_type_int32;
        cap_item.value.resize(sizeof(limit));
        memcpy(&cap_item.value[0], &limit, sizeof(limit));
        easy_codec_item items[] = {limit_item,
                                   start_item,
                                   cap_item};
        req = new EasyCodecItems(3, items);
        uint32_t len;
        byte *reqBytes = easy_marshal(req, &len);
        std::string resp;
        bool ok = syscall("NewIterator", reqBytes, len, &resp);
        delete (req);
        if (!ok)
        {
            return false;
        }
        return true;
    }
}
